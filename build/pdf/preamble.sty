%==============================================================================
% This package takes care of setting up the preamble for the entire document.
% Based on the TeX files from Synthesizing Software From a ForSyDe Model Targeting GPGPUs by Gabriel Hjort Blindell.
%==============================================================================
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{preamble}

%==============================================================================
% PACKAGES TO LOAD
%==============================================================================
% font
\RequirePackage[utf8]{inputenc} % Tells latex the editor uses utf-8
\RequirePackage{kpfonts}
\RequirePackage[varqu]{inconsolata}
\RequirePackage[T1]{fontenc} % Better font encodings in pdf
\RequirePackage[final]{microtype}
\RequirePackage{lettrine}
\RequirePackage{textcase}
\RequirePackage[english]{babel}
% graphics and tables
\RequirePackage{graphicx}
\RequirePackage{float} % Used for placement of graphics (H)
\RequirePackage{tabularx} % initially used for copyright page
\RequirePackage{array}
\RequirePackage{multirow}
\RequirePackage{tikz}
% math
\RequirePackage{amsmath, amsfonts, amssymb, amsthm}
\RequirePackage{xfrac}
\RequirePackage{siunitx}
% other
\RequirePackage[backend=biber,style=authoryear,maxcitenames=3,maxbibnames=99,natbib=true]{biblatex}
\RequirePackage{csquotes}
\RequirePackage[inline,shortlabels]{enumitem}
\RequirePackage[bottom]{footmisc}
\RequirePackage{xspace}
\RequirePackage{ifthen}
\RequirePackage{relsize}
\RequirePackage{xstring}
\RequirePackage{datetime}
\RequirePackage{color}
\RequirePackage{calc}
\RequirePackage{ragged2e}
\RequirePackage{hyperref} % Load hyperref package second to last
\RequirePackage[all]{hypcap} % document references transport to figure, not just caption

%==============================================================================
%    PACKAGE SETTINGS
%==============================================================================
% BibLaTeX
\bibliography{bibliography.bib} % Specify file
\defbibheading{bibliography}{\bibsection} % Fix header problem
\renewbibmacro{in:}{} % remove in: in journal
% Make URLs in bibliography entries smaller
\AtBeginBibliography{\def\UrlFont{\small\ttfamily}}
% redefine date fallbacks for nodate cases
\DeclareLabeldate{\field{date}\field{eventdate}\field{origdate}\literal{nodate}}

% Memoir
\abstractnum   % Format heading as chapter
\abstractintoc % Include "Abstract" in ToC
\newsubfloat{figure} % Creates commands for subfigures
\newsubfloat{table} % Creates commands for subtables

%==============================================================================
%    NEW COMMANDS AND ENVIRONMENTS
%==============================================================================
% Taken from default rmarkdown render rmd to latex for R code highlighting.
\usepackage{color}
\usepackage{fancyvrb}
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\usepackage{framed}
\definecolor{shadecolor}{RGB}{248,248,248}
\newenvironment{Shaded}{\begin{snugshade}}{\end{snugshade}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{{#1}}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{{#1}}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{{#1}}}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{{#1}}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{{#1}}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{{#1}}}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{{#1}}}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{0.94,0.16,0.16}{{#1}}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{{#1}}}
\newcommand{\RegionMarkerTok}[1]{{#1}}
\newcommand{\ErrorTok}[1]{\textbf{{#1}}}
\newcommand{\NormalTok}[1]{{#1}}

%==============================================================================
%    DOCUMENT CUSTOMIZATIONS
%==============================================================================
% Set document left and right margins to equal ratio
%\setlrmargins{*}{*}{1}
%\setlrmargins{*}{*}{1}
\setulmarginsandblock{\myuppermargin}{\mylowermargin}{*}
\setlrmarginsandblock{\myleftmargin}{\myrightmargin}{*}
\checkandfixthelayout

% reduce space before and after floats
% \floatsep % global space between floats
\setlength{\intextsep}{1\baselineskip plus 5.0pt minus 2.0pt} % Space above and below here [h] floats
\setlength{\textfloatsep}{0.5\baselineskip plus 2.0pt minus 2.0pt} % Space below top [t] and above bottom [b] floats
\setlength{\dbltextfloatsep}{0.5\baselineskip plus 2.0pt minus 2.0pt} % like \textfloatsep for double column floats

% Define colors
\newcommand{\titleColor}{\color[rgb]{0.616, 0.0627, 0.176}}
\newcommand{\partChapterNumColor}{\titleColor}
\newcommand{\dropCapColor}{\titleColor}
\newcommand{\tocPageColor}{\color[rgb]{0.0980, 0.329, 0.651}}

% Hyperlink Settings
\definecolor{citecolor}{rgb}{0.616, 0.0627, 0.176}
\definecolor{linkcolor}{rgb}{0.616, 0.0627, 0.176}
\definecolor{urlcolor}{rgb}{0.0980, 0.329, 0.651}
\hypersetup{pdfborder={0 0 0}, colorlinks, citecolor=citecolor, linkcolor=linkcolor, urlcolor=urlcolor}
\hypersetup{pdftitle={\mytitle},pdfauthor={\myauthor},pdfkeywords={\mykeywords},pdfsubject={\mysubject}}

%==============================
% Customize heading appearances
%==============================
% size and style of part and chapter fancy numbers
\newlength{\partNumSizePt}\setlength{\partNumSizePt}{60pt}
\newlength{\chapterNumSizePt}\setlength{\chapterNumSizePt}{60pt}
\newcommand{\partNumSize}{\fontsize{\partNumSizePt}{1.2\partNumSizePt}\selectfont}
\newcommand{\partNumStyle}{\partChapterNumColor}
\newcommand{\chapterNumSize}{\fontsize{\chapterNumSizePt}{1.2\chapterNumSizePt}\selectfont}
\newcommand{\chapterNumStyle}{\partChapterNumColor}
% Customize parts
\renewcommand{\partnamefont}{\Huge\lsstyle\scshape}
\renewcommand{\partnumfont}{\partNumSize\partNumStyle}
\renewcommand{\printpartname}{}
\renewcommand{\printparttitle}[1]{\normalfont\normalcolor\partnamefont #1}
% Customize chapters
\newcommand{\thinRule}{\rule{\textwidth}{0.25pt}}
\setlength{\beforechapskip}{0pt}
\renewcommand*{\chapterheadstart}{\vspace*{\beforechapskip}}
\setlength{\afterchapskip}{0ex}
\setlength{\midchapskip}{3ex}
\renewcommand*{\chapnamefont}{\Large\flushright\lsstyle\scshape\partChapterNumColor}
\renewcommand*{\chapnumfont}{\chapterNumSize\chapterNumStyle}
\renewcommand*{\chaptitlefont}{\normalfont\flushleft\normalcolor\Huge\lsstyle\scshape}
\makeatletter
\renewcommand*{\printchaptername}{\chapnamefont\MakeTextLowercase{\@chapapp}}
\makeatother
\renewcommand*{\chapternamenum}{\quad}
\renewcommand*{\printchapternum}{\chapnumfont\textls[-75]{\classicstylenums{\thechapter}}}
\renewcommand*{\printchaptertitle}[1]{\chaptitlefont #1 \vspace*{2ex}\thinRule}
% Customize sec, subsec, subsubsec
  % lsstyle turns on spacing for smallcaps, but it needs to be turned off for section numbers
  % by using \textls[0] in setsecnumformat. the italic subsection style needs to be balanced with
  % the normalfont command in secnumformat to keep numbers non-italic.
\setsecnumformat{\textls*[0]{\normalfont \fontfamily{jkpos}\selectfont \csname the#1\endcsname\quad}}
\setsecheadstyle{\Large\lsstyle\scshape\MakeTextLowercase}
\setsubsecheadstyle{\itshape}
\setsubsubsecheadstyle{\itshape}
\setbeforesecskip{2.5ex plus 1ex minus 0.2ex}
\setbeforesubsecskip{2.5ex plus 1ex minus 0.2ex}
\setbeforesubsubsecskip{2ex plus 1ex minus 0.2ex}
\setsubsubsecindent{2\parindent}
\setsecnumdepth{subsection} % number sections up to subsections

%====================================
% Customize "Table of ..." appearance
%====================================
% include up to subsections
\setcounter{tocdepth}{2}
% Customize ToC headings
\renewcommand{\cftpartfont}{\partChapterNumColor\lsstyle\scshape}
\renewcommand{\cftchapterfont}{\lsstyle\scshape}
\renewcommand{\cftsectionfont}{}
\renewcommand{\cftsubsectionfont}{}
\renewcommand{\cftfigurefont}{}
\renewcommand{\cfttablefont}{}
% Increase number width
\newlength{\cftNumWidthIncrease}\setlength{\cftNumWidthIncrease}{0.25em}
\addtolength{\cftpartnumwidth}{\cftNumWidthIncrease}
\addtolength{\cftchapternumwidth}{\cftNumWidthIncrease}
\addtolength{\cftsectionindent}{\cftNumWidthIncrease}
\addtolength{\cftsubsectionindent}{\cftNumWidthIncrease}
% No leader dots
\renewcommand*{\cftpartdotsep}{\cftnodots}
\renewcommand*{\cftchapterdotsep}{\cftnodots}
\renewcommand*{\cftsectiondotsep}{\cftnodots}
\renewcommand*{\cftsubsectiondotsep}{\cftnodots}
\renewcommand*{\cftfiguredotsep}{\cftnodots}
\renewcommand*{\cfttabledotsep}{\cftnodots}
% Set page numbers immediately after entry text
\newcommand{\tocEntryPageSep}{\hspace{1em}}
\renewcommand{\cftpartleader}{\tocEntryPageSep}
\renewcommand{\cftpartafterpnum}{\cftparfillskip}
\renewcommand{\cftchapterleader}{\tocEntryPageSep}
\renewcommand{\cftchapterafterpnum}{\cftparfillskip}
\renewcommand{\cftsectionleader}{\tocEntryPageSep}
\renewcommand{\cftsectionafterpnum}{\cftparfillskip}
\renewcommand{\cftsubsectionleader}{\tocEntryPageSep}
\renewcommand{\cftsubsectionafterpnum}{\cftparfillskip}
\renewcommand{\cftfigureleader}{\tocEntryPageSep}
\renewcommand{\cftfigureafterpnum}{\cftparfillskip}
\renewcommand{\cfttableleader}{\tocEntryPageSep}
\renewcommand{\cfttableafterpnum}{\cftparfillskip}
% Customize page numbers
\newcommand{\tocPageStyle}{\fontfamily{jkpos}\selectfont}
\renewcommand{\cftpartpagefont}{\tocPageStyle}
\renewcommand{\cftchapterpagefont}{\tocPageStyle}
\renewcommand{\cftsectionpagefont}{\tocPageStyle}
\renewcommand{\cftsubsectionpagefont}{\tocPageStyle}
\renewcommand{\cftfigurepagefont}{\tocPageStyle}
\renewcommand{\cfttablepagefont}{\tocPageStyle}
% Customize sectioning numbers
\renewcommand{\cftpartpresnum}{\fontfamily{jkpos}\selectfont}
\renewcommand{\cftchapterpresnum}{\fontfamily{jkpos}\selectfont}
\renewcommand{\cftsectionpresnum}{\fontfamily{jkpos}\selectfont}
\renewcommand{\cftsubsectionpresnum}{\fontfamily{jkpos}\selectfont}

%==============================
% Customize headers and footers
%==============================
% For normal pages
\newcommand{\hfTextSize}{\footnotesize}
\newcommand{\headTextStyle}{\fontfamily{jkpos}\selectfont\lsstyle\scshape\MakeTextLowercase}
\nouppercaseheads
\makeevenhead{headings}%
             {\hfTextSize\fontfamily{jkpos}\selectfont\thepage}%
             {}%
             {\hfTextSize\headTextStyle\leftmark}
\makeoddhead{headings}%
            {\hfTextSize\headTextStyle\rightmark}%
            {}%
            {\hfTextSize\fontfamily{jkpos}\selectfont\thepage}
\pagestyle{headings} % turn on headings pagestyle
% For chapter, part, and appenix style pages
\makepagestyle{mychapterstyle}
\makeevenfoot{mychapterstyle}{}{\hfTextSize\fontfamily{jkpos}\selectfont\thepage}{}
\makeoddfoot{mychapterstyle}{}{\hfTextSize\fontfamily{jkpos}\selectfont\thepage}{}
\aliaspagestyle{chapter}{mychapterstyle} % turn on chapter footer style for chapters
\aliaspagestyle{part}{mychapterstyle} % turn on chapter footer style for parts
% Clear style for title page (no header nor footer)
\makepagestyle{titlepage}

%==============================
% Lists (itemize and enumerate)
%==============================
% Lengths
\newlength{\listAllLeftMarginI}\setlength{\listAllLeftMarginI}{1.5\parindent}
\newlength{\listItemizeLeftMarginI}\setlength{\listItemizeLeftMarginI}{\listAllLeftMarginI}
\newlength{\listItemizeLeftMarginII}\setlength{\listItemizeLeftMarginII}{\parindent}
\newlength{\listEnumerateLeftMarginI}\setlength{\listEnumerateLeftMarginI}{\listAllLeftMarginI}
\newlength{\listEnumerateLeftMarginII}\setlength{\listEnumerateLeftMarginII}{\parindent}
\addtolength{\listEnumerateLeftMarginII}{4pt}
\newlength{\listTopSep}\setlength{\listTopSep}{0.25\baselineskip plus 2.0pt minus 2.0pt}
\newlength{\listParSep}\setlength{\listParSep}{0.15\baselineskip plus 1.0pt minus 1.0pt} % space between items
%\newlength{\listLabelSep}\setlength{\listLabelSep}{0.6em} % hspace of item symbol
\newlength{\listItemSep}\setlength{\listItemSep}{0.5\baselineskip}
% Labels
%\newcommand{\listItemizeLabelI}{\large\textbullet}
\newcommand{\listItemizeLabelII}{\textendash}
\newcommand{\listEnumerateLabelI}{{\arabic*}.}
\newcommand{\listEnumerateLabelII}{{\alph*}.}
\newcommand{\listInlineEnumerateLabel}{({\roman*})}
% Global list settings
\setlist{noitemsep, topsep = \listTopSep, parsep = \listParSep, leftmargin = \listAllLeftMarginI}
%\setlist[itemize, 1]{label = \listItemizeLabelI}
\setlist[itemize, 2]{label = \listItemizeLabelII, leftmargin = \listItemizeLeftMarginII, topsep = 0em}
\setlist[enumerate, 1]{label = \listEnumerateLabelI}
\setlist[enumerate, 2]{label = \listEnumerateLabelII, leftmargin = \listEnumerateLeftMarginII, topsep = 0em}
